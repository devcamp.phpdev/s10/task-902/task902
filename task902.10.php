<?php
//Khai báo class person với các thuộc tính: id, firtsName, lastName, dateOfBirth và một phương thức displayFullName để hiển thị tên đầy đủ của object

class person {
    public $id;
    public $firstName;
    public $lastName;
    public $dateOfBirth;
    public function displayFullName(){
        echo "<br/>" . $this->firstName . " " . $this->lastName;
    }
}

// Tạo một object personJoe là thể hiện của class person, thiết lập firstname = John, lastName = Doe, dateOfBirth = 2005-12-28 và id = 123456. Gọi phương thức displayFullName

$personJoe = new person();
$personJoe->firstName = "John";
$personJoe->lastName = "Doe";
$personJoe->dateOfBirth = "2005-12-28";
$personJoe->id = "123456";
$personJoe->displayFullName();

//Tạo một object personJane là thể hiện của class person, thiết lập firstname = Jenny, lastName = Jane, dateOfBirth = 2007-05-28 và id = 123457. Gọi phương thức displayFullName
$personJane = new person();
$personJane->firstName = "Jenny";
$personJane->lastName = "Jane";
$personJane->dateOfBirth = "2007-05-28";
$personJane->displayFullName();
