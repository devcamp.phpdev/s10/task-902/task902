<?php
// class person
class person {
    public $id;
    public $firstName;
    public $lastName;
    public $dateOfBirth;
    public function displayFullName(){
        echo "<br/>" . $this->firstName . " " . $this->lastName;
    }
}
// Khai báo class student kế thừa từ class person ở trên với các thuộc tính mới: className, schoolName, schoolAddress
class student extends person{
    public $className;
    public $schoolName;
    public $schoolAddress;
    /*
        Tạo các phương thức sau:
Phương thức constructor() để thiết lập giá trị ban đầu cho các thuộc tính
Phương thức displaySchoolAddress để hiển thị địa chỉ trường
Phương thức displayStudentInfo để hiển thị toàn bộ thông tin của đối tượng (có sử dụng phương thức displaySchoolAddress().

    */
    public function __construct ($id, $firstName, $lastName, $dateOfBirth, $className, $schoolName, $schoolAddress){
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = $dateOfBirth;
        $this->className = $className;
        $this->schoolName = $schoolName;
        $this->schoolAddress = $schoolAddress;
    }
    public function displaySchoolAddress(){
        echo "<br/>Địa chỉ trường: " . $this->schoolAddress;
    }
    public function displayStudentInfo(){
        echo <<<EOD
        <p>ID: {$this->id}
        <br/>Full name: {$this->firstName} {$this->lastName}
        <br/>Date of birth: {$this->dateOfBirth}
        <br/>Class: {$this->className}
        <br/>School: {$this->schoolName}
EOD;
        $this->displaySchoolAddress();
    }
}

$studentJoe = new student("123456", "John", "Doe", "2004-12-23", "12A", "Nguyễn Tất Thành", "136 Xuân Thủy, Cầu Giấy");
$studentJoe->displayStudentInfo();