<?php
// Khai báo abstract class person với các thuộc tính:id, firtsName, lastName, dateOfBirth và một abstract method getFullName 

abstract class person{
    public $id;
    public $firstName;
    public $lastName;
    public $dateOfBirth;
    abstract public function getFullName();
}

// Khai báo class students kế thừa từ abstract class person ở trên với các thuộc tính mới: className, schoolName, schoolAddress
class student extends person{
    public $className;
    public $schoolName;
    public $schoolAddress;
    /*
        Định nghĩa phương thức constructor để thiết lập giá trị ban đầu cho các thuộc tính.
Định nghĩa lại method getFullName
Định nghĩa phương thức displaySchoolAddress để hiển thị địa chỉ trường
Định nghĩa phương thức displayStudentInfo để hiển thị toàn bộ thông tin của đối tượng (sử dụng 2 phương thức trên).

    */
    public function __construct ($id, $firstName, $lastName, $dateOfBirth, $className, $schoolName, $schoolAddress){
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = $dateOfBirth;
        $this->className = $className;
        $this->schoolName = $schoolName;
        $this->schoolAddress = $schoolAddress;
    }
    public function getFullName(){
        return $this->firstName . " " . $this->lastName;
    }
    public function displaySchoolAddress(){
        echo "<br/>Địa chỉ trường: " . $this->schoolAddress;
    }
    public function displayStudentInfo(){
        $fullName = $this->getFullName();
        echo <<<EOD
        <p>ID: {$this->id}
        <br/>Full name: {$fullName}
        <br/>Date of birth: {$this->dateOfBirth}
        <br/>Class: {$this->className}
        <br/>School: {$this->schoolName}
EOD;
        $this->displaySchoolAddress();
    }

}

$student1 = new student("123456", "Lê Nguyên", "Sinh", "2004-12-23", "12A", "Nguyễn Tất Thành", "136 Xuân Thủy, Cầu Giấy");
$student1->displayStudentInfo();