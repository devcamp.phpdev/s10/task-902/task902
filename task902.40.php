<?php
// Khai báo interface person với 2 abstract method setPersonInfo ($firstName, $lastName, $dateOfBirth) và displayUserInfo(); 
interface person{
    function setPersonInfo ($firstName, $lastName, $dateOfBirth);
    function displayUserInfo();
}
// Khai báo class student implements interface person ở trên với các thuộc tính: $firstName, $lastName, $dateOfBirth
class student implements person{
    public $firstName;
    public $lastName;
    public $dateOfBirth;
    /*
    Định nghĩa constructor
Định nghĩa lại method setPersonInfo và truyền các tham số vào thành các giá trị của các thuộc tính tương ứng.
Định nghĩa lại phương thức displayPersonInfo để hiển thị thông tin của object
    */
    public function __construct ($firstName, $lastName, $dateOfBirth){
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = $dateOfBirth;
    }
    function setPersonInfo ($firstName, $lastName, $dateOfBirth){
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = $dateOfBirth;
    }
    function displayUserInfo(){
        echo <<<EOD
        Fullname: {$this->firstName} {$this->lastName}
        <br/>Date of birth: {$this->lastName}
EOD;
    }
}

/*Tạo một biến object $student1 từ class students, gọi phương thức setPersonInfo () để truyền giá trị cho các thuộc tính và gọi phương thức displayPersonInfo () để hiển thị thông tin ra trình duyệt */
$student1 = new student("","","");
$student1->setPersonInfo("Lê Nguyên", "Sinh", "1987-06-05");
$student1->displayUserInfo();
